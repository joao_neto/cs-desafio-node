const { MONGO_DB, NODE_ENV } = process.env;

const config = {
  development: {
    database: 'mongodb://localhost:27017/cs-desafio-node'
  },

  production: {
    database: MONGO_DB
  }
};

export default config[NODE_ENV || 'development'];
