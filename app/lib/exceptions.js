class BaseException extends Error {
  constructor(msg, code) {
    super(msg);
    this.code = code;
  }
}

export class BadRequest extends BaseException {
  constructor(msg = 'Bad Request', code = 'bad_request') {
    super(msg, code);
    this.status = 400;
  }
}

export class Unauthorized extends BaseException {
  constructor(msg = 'Unauthorized', code = 'unauthorized') {
    super(msg, code);
    this.status = 401;
  }
}

export class Forbidden extends BaseException {
  constructor(msg = 'Forbidden', code = 'forbidden') {
    super(msg, code);
    this.status = 403;
  }
}

export class NotFound extends BaseException {
  constructor(msg = 'Not Found', code = 'not_found') {
    super(msg, code);
    this.status = 404;
  }
}

export class TooManyRequests extends BaseException {
  constructor(msg = 'Too Many Requests', code = 'too_many_requests') {
    super(msg, code);
    this.status = 429;
  }
}

export class InternalServiceError extends BaseException {
  constructor(msg = 'Internal Service Error', code = 'internal_service_error') {
    super(msg, code);
    this.status = 500;
  }
}
